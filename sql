DROP TABLE IF EXISTS sand_cast.account_coverage;
CREATE TABLE sand_cast.account_coverage as

SELECT

--Account Attributes
kc.corp_par_nm
,kc.ownr_sub_nm
,kc.customer_group_name
,kc.gpo_med_surgcl_nm
,kc.gpo_pharml_nm
,kc.cot_clas_desc
,kc.cot_fclt_typ_desc
,kc.cot_spcl_desc
,kc.prim_mkt_desc
,kc.customer_market_segment_desc
,kc.bus_nm
,kc.addr_ln_1_txt
,kc.addr_ln_2_txt
,kc.city_nm
,kc.st_cd
,kc.zipcode
,kc.customer_account_number
,kc.customer_site_use_id
,kc.customer_site_use_code
,kc.sap_id
,kc.telephn_nbr
,kc.web_url_id

--Key Account Teams

,kc.corp_par_mgr
,kc.corp_par_team
,kc.ownr_sub_mgr
,kc.ownr_sub_team
,kc.customer_group_mgr
,kc.customer_group_team


--Acute FST/IST

--ACI Inside Sales Rep Acute
,zt.aci_region      
,zt.aci_district    
,zt.aci_territory_code 
,zt.aci_salesrep_name  
,zt.aci_dm_name     
,zt.aci_rvp_name    
--ATR Patient Support Specialist
,zt.atr_region
,zt.atr_district
,zt.atr_territory_code
,zt.atr_salesrep_name
,zt.atr_dm_name
,zt.atr_rvp_name
--BDM Prevena BDM
,zt.bdm_region
,zt.bdm_district
,zt.bdm_territory_code
,zt.bdm_salesrep_name
,zt.bdm_dm_name
,zt.bdm_rvp_name
--SSR Surgical Solutions Account Manager 
,zt.ssr_region
,zt.ssr_district
,zt.ssr_territory_code
,zt.ssr_salesrep_name
,zt.ssr_dm_name
,zt.ssr_rvp_name
--TMV Acute Sales Manager Alaska
,zt.tmv_region
,zt.tmv_district
,zt.tmv_territory_code
,zt.tmv_salesrep_name
,zt.tmv_dm_name
,zt.tmv_rvp_name

--,case when zt.atr_dm_name is null then zt.tmv_dm_name else zt.atr_dm_name end as 'atr/tmv_rsm'
--,case when zt.atr_salesrep_name is null then zt.tmv_salesrep_name else zt.atr_salesrep_name end as 'atr/tmv_rep'
--,case when zt.awt_dm_name is null then zt.tsv_dm_name else zt.awt_dm_name end as 'awt/tsv_rsm'
--,case when zt.awt_salesrep_name is null then zt.tsv_salesrep_name else zt.atr_salesrep_name end as 'awt/tsv_rep'


--WHS Wound Healing Solutions Account Manager
,zt.whs_region
,zt.whs_district
,zt.whs_territory_code
,zt.whs_salesrep_name
,zt.whs_dm_name
,zt.whs_rvp_name

--OOH FST/IST

--ASM Ambulatory Surgery Center Account Manager
,zt.asm_region
,zt.asm_district
,zt.asm_territory_code
,zt.asm_salesrep_name
,zt.asm_dm_name
,zt.asm_rvp_name
--AWT AWT Therapy Solutions Account Manager
,zt.awt_region
,zt.awt_district
,zt.awt_territory_code
,zt.awt_salesrep_name
,zt.awt_dm_name
,zt.awt_rvp_name
--ASI Inside Sales Rep (ASC)
,zt.asi_region
,zt.asi_district
,zt.asi_territory_code
,zt.asi_salesrep_name
,zt.asi_dm_name
,zt.asi_rvp_name
--OSI Inside Sales Rep (HHA/WCC)
,zt.osi_region
,zt.osi_district
,zt.osi_territory_code
,zt.osi_salesrep_name
,zt.osi_dm_name
,zt.osi_rvp_name
--ISR Inside Sales Rep (SNF)
,zt.isr_region
,zt.isr_district
,zt.isr_territory_code
,zt.isr_salesrep_name
,zt.isr_dm_name
,zt.isr_rvp_name
--TSR Inside Sales Transitions Rep (SNF)
,zt.tsr_region
,zt.tsr_district
,zt.tsr_territory_code
,zt.tsr_salesrep_name
,zt.tsr_dm_name
,zt.tsr_rvp_name
--TSV OOH Sales Manager Alaska
,zt.tsv_region
,zt.tsv_district
,zt.tsv_territory_code
,zt.tsv_salesrep_name
,zt.tsv_dm_name
,zt.tsv_rvp_name


FROM sand_gldrill.test_kam_customer_master kc


LEFT JOIN (SELECT zip_code

--ACUTE COVERAGE

--ACI Inside Sales Rep Acute
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ACI' THEN region END),'No Coverage') AS aci_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ACI' THEN district END),'No Coverage') AS aci_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ACI' THEN territory_code END),'No Coverage') AS aci_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ACI' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS aci_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ACI' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS aci_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ACI' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS aci_rvp_name
--ATR Patient Support Specialist
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ATR' THEN region END),'No Coverage') AS atr_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ATR' THEN district END),'No Coverage') AS atr_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ATR' THEN territory_code END),'No Coverage') AS atr_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ATR' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS atr_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ATR' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS atr_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ATR' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS atr_rvp_name
--BDM Prevena BDM
,ifnull(MAX(CASE sales_credit_type_code WHEN 'BDM' THEN region END),'No Coverage') AS bdm_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'BDM' THEN district END),'No Coverage') AS bdm_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'BDM' THEN territory_code END),'No Coverage') AS bdm_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'BDM' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS bdm_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'BDM' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS bdm_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'BDM' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS bdm_rvp_name
--SSR Surgical Solutions Account Manager 
,ifnull(MAX(CASE sales_credit_type_code WHEN 'SSR' THEN region END),'No Coverage') AS ssr_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'SSR' THEN district END),'No Coverage') AS ssr_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'SSR' THEN territory_code END),'No Coverage') AS ssr_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'SSR' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS ssr_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'SSR' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS ssr_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'SSR' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS ssr_rvp_name
--TMV Acute Sales Manager Alaska
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TMV' THEN region END),'No Coverage') AS tmv_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TMV' THEN district END),'No Coverage') AS tmv_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TMV' THEN territory_code END),'No Coverage') AS tmv_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TMV' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS tmv_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TMV' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS tmv_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TMV' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS tmv_rvp_name
--WHS Wound Healing Solutions Account Manager
,ifnull(MAX(CASE sales_credit_type_code WHEN 'WHS' THEN region END),'No Coverage') AS whs_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'WHS' THEN district END),'No Coverage') AS whs_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'WHS' THEN territory_code END),'No Coverage') AS whs_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'WHS' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS whs_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'WHS' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS whs_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'WHS' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS whs_rvp_name

--OOH COVERAGE

--ASM Ambulatory Surgery Center Account Manager
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASM' THEN region END),'No Coverage') AS asm_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASM' THEN district END),'No Coverage') AS asm_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASM' THEN territory_code END),'No Coverage') AS asm_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASM' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS asm_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASM' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS asm_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASM' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS asm_rvp_name
--AWT Therapy Solutions Account Manager
,ifnull(MAX(CASE sales_credit_type_code WHEN 'AWT' THEN region END),'No Coverage') AS awt_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'AWT' THEN district END),'No Coverage') AS awt_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'AWT' THEN territory_code END),'No Coverage') AS awt_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'AWT' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS awt_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'AWT' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS awt_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'AWT' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS awt_rvp_name
--ASI Inside Sales Rep (ASC)
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASI' THEN region END),'No Coverage') AS asi_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASI' THEN district END),'No Coverage') AS asi_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASI' THEN territory_code END),'No Coverage') AS asi_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASI' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS asi_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASI' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS asi_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ASI' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS asi_rvp_name
--OSI Inside Sales Rep (HHA/WCC)
,ifnull(MAX(CASE sales_credit_type_code WHEN 'OSI' THEN region END),'No Coverage') AS osi_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'OSI' THEN district END),'No Coverage') AS osi_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'OSI' THEN territory_code END),'No Coverage') AS osi_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'OSI' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS osi_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'OSI' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS osi_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'OSI' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS osi_rvp_name
--ISR Inside Sales Rep (SNF)
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ISR' THEN region END),'No Coverage') AS isr_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ISR' THEN district END),'No Coverage') AS isr_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ISR' THEN territory_code END),'No Coverage') AS isr_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ISR' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS isr_salesrep_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'ISR' THEN (CASE sup_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE sup_name END) END),'No Coverage') AS isr_sup_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ISR' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS isr_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'ISR' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS isr_rvp_name
--TSR Inside Sales Transitions Rep (SNF)
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSR' THEN region END),'No Coverage') AS tsr_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSR' THEN district END),'No Coverage') AS tsr_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSR' THEN territory_code END),'No Coverage') AS tsr_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSR' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS tsr_salesrep_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSR' THEN (CASE sup_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE sup_name END) END),'No Coverage') AS tsr_sup_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSR' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS tsr_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSR' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS tsr_rvp_name
--TSV OOH Sales Manager Alaska
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSV' THEN region END),'No Coverage') AS tsv_region
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSV' THEN district END),'No Coverage') AS tsv_district
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSV' THEN territory_code END),'No Coverage') AS tsv_territory_code
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSV' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS tsv_salesrep_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSV' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS tsv_dm_name
,ifnull(MAX(CASE sales_credit_type_code WHEN 'TSV' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS tsv_rvp_name

----OTHER CODES

----WCR
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'WCR' THEN region END),'No Coverage') AS wcr_region
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'WCR' THEN district END),'No Coverage') AS wcr_district
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'WCR' THEN territory_code END),'No Coverage') AS wcr_territory_code
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'WCR' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS wcr_salesrep_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'WCR' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS wcr_dm_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'WCR' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS wcr_rvp_name
----VCS
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'VCS' THEN region END),'No Coverage') AS vcs_region
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'VCS' THEN district END),'No Coverage') AS vcs_district
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'VCS' THEN territory_code END),'No Coverage') AS vcs_territory_code
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'VCS' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS vcs_salesrep_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'VCS' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS vcs_dm_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'VCS' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS vcs_rvp_name
----AES
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'AES' THEN region END),'No Coverage') AS aes_region
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'AES' THEN district END),'No Coverage') AS aes_district
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'AES' THEN territory_code END),'No Coverage') AS aes_territory_code
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'AES' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS aes_salesrep_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'AES' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS aes_dm_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'AES' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS aes_rvp_name
----RCS
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'RCS' THEN region END),'No Coverage') AS rcs_region
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'RCS' THEN district END),'No Coverage') AS rcs_district
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'RCS' THEN territory_code END),'No Coverage') AS rcs_territory_code
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'RCS' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS rcs_salesrep_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'RCS' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS rcs_dm_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'RCS' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS rcs_rvp_name
----SVA
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'SVA' THEN region END),'No Coverage') AS sva_region
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'SVA' THEN district END),'No Coverage') AS sva_district
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'SVA' THEN territory_code END),'No Coverage') AS sva_territory_code
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'SVA' THEN (CASE salesrep_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE salesrep_name END) END),'No Coverage') AS sva_salesrep_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'SVA' THEN (CASE dm_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE dm_name END) END),'No Coverage') AS sva_dm_name
--,ifnull(MAX(CASE sales_credit_type_code WHEN 'SVA' THEN (CASE rvp_name WHEN 'KINETIC CONCEPTS' THEN 'OPEN' ELSE rvp_name END) END),'No Coverage') AS sva_rvp_name



FROM aim.aim_zip_terr_rep_dim WHERE current_align_flag = 'Y'

GROUP BY zip_code) zt ON zt.zip_code = kc.zip5_cd

--WHERE kc.corp_par_nm LIKE '%SENTARA%'
--WHERE kc.ownr_sub_nm LIKE '%%'
--WHERE kc.customer_group_name LIKE '%%'


GROUP BY

--Account Attributes
kc.corp_par_nm
,kc.ownr_sub_nm
,kc.customer_group_name
,kc.gpo_med_surgcl_nm
,kc.gpo_pharml_nm
,kc.cot_clas_desc
,kc.cot_fclt_typ_desc
,kc.cot_spcl_desc
,kc.prim_mkt_desc
,kc.customer_market_segment_desc
,kc.bus_nm
,kc.addr_ln_1_txt
,kc.addr_ln_2_txt
,kc.city_nm
,kc.st_cd
,kc.zipcode
,kc.customer_account_number
,kc.customer_site_use_id
,kc.customer_site_use_code
,kc.sap_id
,kc.telephn_nbr
,kc.web_url_id

--Key Account Teams

,kc.corp_par_mgr
,kc.corp_par_team
,kc.ownr_sub_mgr
,kc.ownr_sub_team
,kc.customer_group_mgr
,kc.customer_group_team


--Acute FST/IST

--ACI Inside Sales Rep Acute
,zt.aci_region      
,zt.aci_district    
,zt.aci_territory_code 
,zt.aci_salesrep_name  
,zt.aci_dm_name     
,zt.aci_rvp_name    
--ATR Patient Support Specialist
,zt.atr_region
,zt.atr_district
,zt.atr_territory_code
,zt.atr_salesrep_name
,zt.atr_dm_name
,zt.atr_rvp_name
--BDM Prevena BDM
,zt.bdm_region
,zt.bdm_district
,zt.bdm_territory_code
,zt.bdm_salesrep_name
,zt.bdm_dm_name
,zt.bdm_rvp_name
--SSR Surgical Solutions Account Manager 
,zt.ssr_region
,zt.ssr_district
,zt.ssr_territory_code
,zt.ssr_salesrep_name
,zt.ssr_dm_name
,zt.ssr_rvp_name
--TMV Acute Sales Manager Alaska
,zt.tmv_region
,zt.tmv_district
,zt.tmv_territory_code
,zt.tmv_salesrep_name
,zt.tmv_dm_name
,zt.tmv_rvp_name

--,case when zt.atr_dm_name is null then zt.tmv_dm_name else zt.atr_dm_name end as 'atr/tmv_rsm'
--,case when zt.atr_salesrep_name is null then zt.tmv_salesrep_name else zt.atr_salesrep_name end as 'atr/tmv_rep'
--,case when zt.awt_dm_name is null then zt.tsv_dm_name else zt.awt_dm_name end as 'awt/tsv_rsm'
--,case when zt.awt_salesrep_name is null then zt.tsv_salesrep_name else zt.atr_salesrep_name end as 'awt/tsv_rep'


--WHS Wound Healing Solutions Account Manager
,zt.whs_region
,zt.whs_district
,zt.whs_territory_code
,zt.whs_salesrep_name
,zt.whs_dm_name
,zt.whs_rvp_name

--OOH FST/IST

--ASM Ambulatory Surgery Center Account Manager
,zt.asm_region
,zt.asm_district
,zt.asm_territory_code
,zt.asm_salesrep_name
,zt.asm_dm_name
,zt.asm_rvp_name
--AWT AWT Therapy Solutions Account Manager
,zt.awt_region
,zt.awt_district
,zt.awt_territory_code
,zt.awt_salesrep_name
,zt.awt_dm_name
,zt.awt_rvp_name
--ASI Inside Sales Rep (ASC)
,zt.asi_region
,zt.asi_district
,zt.asi_territory_code
,zt.asi_salesrep_name
,zt.asi_dm_name
,zt.asi_rvp_name
--OSI Inside Sales Rep (HHA/WCC)
,zt.osi_region
,zt.osi_district
,zt.osi_territory_code
,zt.osi_salesrep_name
,zt.osi_dm_name
,zt.osi_rvp_name
--ISR Inside Sales Rep (SNF)
,zt.isr_region
,zt.isr_district
,zt.isr_territory_code
,zt.isr_salesrep_name
,zt.isr_dm_name
,zt.isr_rvp_name
--TSR Inside Sales Transitions Rep (SNF)
,zt.tsr_region
,zt.tsr_district
,zt.tsr_territory_code
,zt.tsr_salesrep_name
,zt.tsr_dm_name
,zt.tsr_rvp_name
--TSV OOH Sales Manager Alaska
,zt.tsv_region
,zt.tsv_district
,zt.tsv_territory_code
,zt.tsv_salesrep_name
,zt.tsv_dm_name
,zt.tsv_rvp_name